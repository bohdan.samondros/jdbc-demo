package org.demo.dao;

import org.demo.domain.Country;

import java.util.List;
import java.util.Optional;

public interface CountryDAO {
    List<Country> getAll();
    Optional<Country> findByCode(String Code);
    boolean delete(String code);
    boolean update(Country country);
}
