package org.demo.dao;

import org.demo.domain.City;
import org.demo.domain.Country;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.demo.Constants.*;

public class JdbcCityDAO implements CityDAO {
    private static final Logger log = LoggerFactory.getLogger(JdbcCityDAO.class);

    @Override
    public List<City> findByCountry(Country country) {
        List<City> result = new ArrayList<>();
        try {
            try (Connection connection = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASSWORD);
                 PreparedStatement statement = connection.prepareStatement("SELECT * FROM city INNER JOIN country WHERE city.CountryCode = ?")
            ) {
                statement.setString(1, country.getCode());

                ResultSet resultSet = statement.executeQuery();

                if (resultSet.next()) {
                    City city = new City();

                    city.setId(resultSet.getLong(1));
                    city.setName(resultSet.getString(2));
                    Country countryResult = new Country();

                    countryResult.setCode(resultSet.getString(6));
                    countryResult.setName(resultSet.getString(7));
                    countryResult.setContinent(resultSet.getString(8));
                    countryResult.setRegion(resultSet.getString(9));
                    countryResult.setPopulation(resultSet.getLong(10));

                    city.setCountry(countryResult);

                    city.setDistrict(resultSet.getString(4));
                    city.setPopulation(resultSet.getLong(5));

                    result.add(city);
                }
            }
        } catch (SQLException ex) {
            log.error(ex.getMessage(), ex);
        }

        return result;
    }
}
