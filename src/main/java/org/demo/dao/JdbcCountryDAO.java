package org.demo.dao;

import org.demo.domain.Country;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.demo.Constants.*;

public class JdbcCountryDAO implements CountryDAO {
    private static final Logger log = LoggerFactory.getLogger(JdbcCountryDAO.class);

    @Override
    public List<Country> getAll() {
        List<Country> countries = new ArrayList<>();
        try {
            try (Connection connection = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASSWORD)) {
                Statement statement = connection.createStatement();

                ResultSet resultSet = statement.executeQuery("SELECT * FROM country");

                while (resultSet.next()) {
                    Country country = new Country();

                    country.setCode(resultSet.getString(1));
                    country.setName(resultSet.getString(2));
                    country.setContinent(resultSet.getString(3));
                    country.setRegion(resultSet.getString(4));
                    country.setPopulation(resultSet.getLong(7));

                    countries.add(country);
                }
            }
        } catch (SQLException ex) {
            log.error(ex.getMessage(), ex);
        }
        return countries;
    }

    @Override
    public Optional<Country> findByCode(String code) {
        Optional<Country> result = Optional.empty();
        try {
            try (Connection connection = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASSWORD)) {
                PreparedStatement statement = connection.prepareStatement("SELECT * FROM country WHERE Code = ?");
                statement.setString(1, code);

                ResultSet resultSet = statement.executeQuery();

                if (resultSet.next()) {
                    Country country = new Country();

                    country.setCode(resultSet.getString(1));
                    country.setName(resultSet.getString(2));
                    country.setContinent(resultSet.getString(3));
                    country.setRegion(resultSet.getString(4));
                    country.setPopulation(resultSet.getLong(7));

                    result = Optional.of(country);
                } else {
                    return Optional.empty();
                }
            }
        } catch (SQLException ex) {
            log.error(ex.getMessage(), ex);
        }

        return result;
    }

    @Override
    public boolean delete(String code) {
        try {
            try (Connection connection = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASSWORD)) {
                PreparedStatement statement = connection.prepareStatement("DELETE FROM country WHERE Code = ?");
                statement.setString(1, code);

                return statement.execute();
            }
        } catch (SQLException ex) {
            log.error(ex.getMessage(), ex);
            return false;
        }
    }

    @Override
    public boolean update(Country country) {
        try {
            try (Connection connection = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASSWORD)) {
                PreparedStatement statement = connection.prepareStatement("UPDATE  country SET Name = ?, Continent = ?, Region = ?, Population = ? WHERE Code = ?");

                statement.setString(1, country.getName());
                statement.setString(2, country.getContinent());
                statement.setString(3, country.getRegion());
                statement.setLong(4, country.getPopulation());

                statement.setString(5, country.getCode());

                return statement.executeUpdate() > 1;
            }
        } catch (SQLException ex) {
            log.error(ex.getMessage(), ex);
            return false;
        }
    }
}
