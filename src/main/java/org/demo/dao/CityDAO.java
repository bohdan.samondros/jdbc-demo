package org.demo.dao;

import org.demo.domain.City;
import org.demo.domain.Country;

import java.util.List;

public interface CityDAO {
    List<City> findByCountry(Country country);
}
