package org.demo;

import com.mysql.cj.jdbc.StatementWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.StringJoiner;

import static org.demo.Constants.*;

public class SimpleQueryDemo {
    //    private static final String DRIVER_NAME = "com.mysql.jdbc.Driver";
    private static final Logger log = LoggerFactory.getLogger(SimpleQueryDemo.class);
    private static final String SELECT_COUNTRIES_SQL = "/sql/select_countries.sql";

    /*    static {
                try {
                    Class.forName(DRIVER_NAME);
                } catch (Exception ex) {
                    throw new RuntimeException(ex);
                }
            }*/
    public static void main(String[] args) {

        try {
            String query = new BufferedReader(new InputStreamReader(SimpleQueryDemo.class.getResourceAsStream(SELECT_COUNTRIES_SQL))).readLine();
            try (Connection connection = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASSWORD);
                 Statement stmnt = connection.createStatement()) {
                ResultSet resultSet = stmnt.executeQuery(query);

                ResultSetMetaData metaData = resultSet.getMetaData();
                StringJoiner tableHeader = new StringJoiner(";", "", "");
                for (int i = 0; i < metaData.getColumnCount(); i++) {
                    tableHeader.add(metaData.getColumnName(i + 1));
                }

                System.out.println(tableHeader);

                while (resultSet.next()) {
                    StringJoiner tableRow = new StringJoiner(";", "", "");
                    for (int i = 0; i < metaData.getColumnCount(); i++) {
                        tableRow.add(resultSet.getString(i + 1));
                    }
                    System.out.println(tableRow);
                }
            }
        } catch (SQLException | IOException ex) {
            log.error(ex.getMessage(), ex);
        }
    }
}
