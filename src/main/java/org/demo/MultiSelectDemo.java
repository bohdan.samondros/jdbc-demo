package org.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.StringJoiner;

import static org.demo.Constants.*;

public class MultiSelectDemo {
    private static final Logger log = LoggerFactory.getLogger(MultiSelectDemo.class);

    public static void main(String[] args) {
        try {
            String query = "SELECT * FROM country LIMIT 1; SELECT * FROM city LIMIT 1;";
            try (Connection connection = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASSWORD);
                 Statement stmnt = connection.createStatement()) {
                stmnt.execute(query);
                ResultSet resultSet = stmnt.getResultSet();
                handleResult(resultSet);
                if (stmnt.getMoreResults()) {
                    handleResult(stmnt.getResultSet());
                }
            }
        } catch (SQLException ex) {
            log.error(ex.getMessage(), ex);
        }
    }

    private static void handleResult(ResultSet resultSet) throws SQLException {
        ResultSetMetaData metaData = resultSet.getMetaData();
        StringJoiner tableHeader = new StringJoiner(";", "", "");
        for (int i = 0; i < metaData.getColumnCount(); i++) {
            tableHeader.add(metaData.getColumnName(i + 1));
        }

        System.out.println(tableHeader);

        while (resultSet.next()) {
            StringJoiner tableRow = new StringJoiner(";", "", "");
            for (int i = 0; i < metaData.getColumnCount(); i++) {
                tableRow.add(resultSet.getString(i + 1));
            }
            System.out.println(tableRow);
        }
    }
}
