package org.demo;

public class Constants {
    public static final String JDBC_URL = "jdbc:mysql://localhost:3306/world?useUnicode=true&useSSL=false&serverTimezone=UTC&allowMultiQueries=true";
    public static final String JDBC_USER = "admin";
    public static final String JDBC_PASSWORD = "admin";
}
