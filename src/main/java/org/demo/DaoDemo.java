package org.demo;

import org.demo.dao.CityDAO;
import org.demo.dao.CountryDAO;
import org.demo.dao.JdbcCityDAO;
import org.demo.dao.JdbcCountryDAO;
import org.demo.domain.Country;

import java.util.List;
import java.util.Optional;

public class DaoDemo {
    public static void main(String[] args) {
        CountryDAO countryDAO = new JdbcCountryDAO();
        List<Country> allCountries = countryDAO.getAll();

        System.out.println(allCountries);
        System.out.println(allCountries.size());

        Optional<Country> country = countryDAO.findByCode("ALB");
        System.out.println("findByCode: " + country);

        CityDAO cityDAO = new JdbcCityDAO();

        System.out.println(cityDAO.findByCountry(country.get()));
    }
}
