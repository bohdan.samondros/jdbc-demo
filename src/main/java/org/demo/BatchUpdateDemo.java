package org.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

import static org.demo.Constants.*;

public class BatchUpdateDemo {
    private static final Logger log = LoggerFactory.getLogger(BatchUpdateDemo.class);

    public static void main(String[] args) {
        try {
            try (Connection connection = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASSWORD)) {
                String query1 = "UPDATE country SET Name = 'ABC' WHERE Code = 'ALB'";
                String query2 = "UPDATE country SET Name = '(Changed)' WHERE Code = 'AFG'";

                Statement stmnt = connection.createStatement();

                stmnt.addBatch(query1);
                stmnt.addBatch(query2);

                int[] recordsAffected = stmnt.executeBatch();

                PreparedStatement preparedStatement = connection.prepareStatement("UPDATE country SET Name = 'ABC' WHERE Code = ?");

                preparedStatement.setString(1, "Code1");

                preparedStatement.addBatch();

                preparedStatement.setString(1, "Code2");
                preparedStatement.addBatch();

                int[] recordsAffected2 = preparedStatement.executeBatch();
            }
        } catch (SQLException ex) {
            log.error(ex.getMessage(), ex);
        }
    }
}
