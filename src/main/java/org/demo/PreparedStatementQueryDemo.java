package org.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.StringJoiner;

import static org.demo.Constants.*;

public class PreparedStatementQueryDemo {
    private static final Logger log = LoggerFactory.getLogger(PreparedStatementQueryDemo.class);
    private static final String SELECT_COUNTRIES_SQL = "/sql/select_countries_by_population.sql";

    public static void main(String[] args) {
        long populationThreshold = 10L;

        try {
            try (Connection connection = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASSWORD)) {
                getCountriesByPopulationGreater(connection, populationThreshold);
            }
        } catch (SQLException | IOException ex) {
            log.error(ex.getMessage(), ex);
        }
    }

    private static void getCountriesByPopulationGreater(Connection connection, long populationThreshold) throws SQLException, IOException {
        String query = new BufferedReader(new InputStreamReader(SimpleQueryDemo.class.getResourceAsStream(SELECT_COUNTRIES_SQL))).readLine();
        try (PreparedStatement stmnt = connection.prepareStatement(query)) {
            stmnt.setLong(1, populationThreshold);
            ResultSet resultSet = stmnt.executeQuery();

            ResultSetMetaData metaData = resultSet.getMetaData();
            StringJoiner tableHeader = new StringJoiner(";", "", "");
            for (int i = 0; i < metaData.getColumnCount(); i++) {
                tableHeader.add(metaData.getColumnName(i + 1));
            }

            System.out.println(tableHeader);

            while (resultSet.next()) {
                StringJoiner tableRow = new StringJoiner(";", "", "");
                for (int i = 0; i < metaData.getColumnCount(); i++) {
                    tableRow.add(resultSet.getString(i + 1));
                }
                System.out.println(tableRow);
            }
        }
    }
}