package org.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

import static org.demo.Constants.*;

public class TransactionUpdateDemo {
    private static final Logger log = LoggerFactory.getLogger(TransactionUpdateDemo.class);

    public static void main(String[] args) {
        try {
            try (Connection connection = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASSWORD)) {
                boolean initialAutocommit = connection.getAutoCommit();
                connection.setAutoCommit(false);
                connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

                String query1 = "UPDATE country SET Name = 'ABC' WHERE Code = 'ALB'";
                String query2 = "UPDATE country SET Name = '(Changed)' WHERE Code = 'AFG'";

                Statement stmnt = connection.createStatement();

                Savepoint savepoint = null;
                try {
                    stmnt.executeUpdate(query1);
                    savepoint = connection.setSavepoint("savepointName");
                    stmnt.executeUpdate(query2);

                    connection.commit();
                } catch (SQLException sqle) {
                    if (savepoint != null) {
                        connection.rollback(savepoint);
                    } else {
                        connection.rollback();
                    }
                } finally {
                    connection.setAutoCommit(initialAutocommit);
                }
            }
        } catch (SQLException ex) {
            log.error(ex.getMessage(), ex);
        }
    }
}
